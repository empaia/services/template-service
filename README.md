# Template Service

This is a service template, that can be copied to initialize a new service repository with a basic code structure. The abbreviation `TS` or `ts` for *Template Service* can be found across the code base. Please change this to fit the name of the new service. 

This template is based on:

* `fastapi` to define HTTP/REST routes
* `asyncpg` for asynchronous access to a PostgreSQL database
* `typer` to setup the CLI command `tsctl` (Template Service Control), providing develop tools (see `tsctl --help`)

## Dev Setup

* install docker
* install docker-compose
* install poetry
* clone template-service

Then run the following commands to create a new virtual environment and install project dependencies.

```bash
sudo apt update
sudo apt install python3-venv python3-pip
cd template-service
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

### Set Environment

Set environment variables in a `.env` file.

```bash
cp sample.env .env  # edit .env if necessary
```

### Run

Start services using `docker-compose`.

```bash
docker-compose up --build -d
```

Or start `template_service` with uvicorn and only a development database with docker-compose.

```bash
docker-compose up -d template-service-db
tsctl migrate-db
uvicorn --host=0.0.0.0 --port=8000 --reload template_service.app:app
```

Access the interactive OpenAPI specification in a browser:

* http://localhost:8000/docs
* http://localhost:8000/v1/docs

### Stop and Remove

```bash
docker-compose down
```

### Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
  * `black` formatting should resolve most issues for `pycodestyle`
* run tests with `pytest`

```bash
isort .
black .
pycodestyle template_service tests
pylint template_service tests
pytest --maxfail=1  # requires service running
```
