from asyncpg.exceptions import UniqueViolationError


async def step_01(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    sql = """
    CREATE TABLE dummy (
        id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
        data jsonb NOT NULL
    );
    """
    await conn.execute(sql)
