from typing import Optional

from asyncpg import Connection

from .models import Dummy, PostDummy
from .singletons import logger


class DummyClient:
    def __init__(self, conn: Connection):
        self.conn = conn

    @staticmethod
    def _sql_job_as_json():
        return """json_build_object(
            'id', id,
            'data', data
        )"""

    async def add(self, post_dummy: PostDummy):
        async with self.conn.transaction():
            sql = f"""
            INSERT INTO dummy (data)
            VALUES ($1)
            RETURNING {DummyClient._sql_job_as_json()};
            """
            logger.debug(sql)

            row = await self.conn.fetchrow(sql, post_dummy.data)
            return Dummy(**row["json_build_object"])

    async def get(self, dummy_id: str) -> Optional[dict]:
        sql = f"""
        SELECT {DummyClient._sql_job_as_json()}
        FROM dummy
        WHERE id=$1;
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, dummy_id)
        if row is None:
            return
        return Dummy(**row["json_build_object"])
