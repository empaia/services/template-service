from typing import Dict

from pydantic import UUID4, BaseModel


class RestrictedBaseModel(BaseModel):
    """Abstract Super-class not allowing unknown fields in the **kwargs."""

    class Config:
        extra = "forbid"


class PostDummy(RestrictedBaseModel):
    data: Dict


class Dummy(PostDummy):
    id: UUID4
