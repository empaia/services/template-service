from fastapi.exceptions import HTTPException
from pydantic import UUID4

from ...db import db_clients
from ...models import Dummy, PostDummy


def add_routes_dummy(app, late_init):
    @app.post(
        "/dummies",
        response_model=Dummy,
        tags=["Dummy"],
    )
    async def _(
        post_dummy: PostDummy,
    ) -> Dummy:
        async with late_init.pool.acquire() as conn:
            dummy_client = await db_clients(conn=conn)
            return await dummy_client.add(post_dummy=post_dummy)

    @app.get(
        "/dummies/{dummy_id}",
        response_model=Dummy,
        tags=["Dummy"],
    )
    async def _(
        dummy_id: UUID4,
    ) -> Dummy:
        async with late_init.pool.acquire() as conn:
            dummy_client = await db_clients(conn=conn)
            dummy = await dummy_client.get(dummy_id=dummy_id)
            if dummy is None:
                raise HTTPException(status_code=404, detail=f"Dummy with id {dummy_id} not found.")
            return dummy
