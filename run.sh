#!/usr/bin/env bash

tsctl migrate-db && uvicorn template_service.app:app $@
