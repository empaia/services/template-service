import uuid

import requests

from .singletons import ts_url


def test_post_get_dummy():
    post_dummy = {"data": {"hello": "world", "answer": 42, "is_working": True}}
    r = requests.post(f"{ts_url}/v1/dummies/", json=post_dummy)
    r.raise_for_status()
    dummy = r.json()
    assert "id" in dummy
    assert "data" in dummy
    assert dummy["data"] == post_dummy["data"]
    dummy_id = dummy["id"]
    uuid.UUID(dummy_id)
    r = requests.get(f"{ts_url}/v1/dummies/{dummy_id}")
    r.raise_for_status()
    dummy = r.json()
    assert "id" in dummy
    assert "data" in dummy
    assert dummy["id"] == dummy_id
    assert dummy["data"] == post_dummy["data"]
    assert isinstance(dummy["data"]["answer"], int)
    assert isinstance(dummy["data"]["is_working"], bool)
