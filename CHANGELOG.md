# 0.2.18

* using fastapi lifespan syntax

# 0.2.15

* renovate

# 0.2.13

* renovate

# 0.2.12

* renovate

# 0.2.11

* renovate

# 0.2.10

* renovate

# 0.2.9

* renovate

# 0.2.8

* renovate

# 0.2.7

* renovate

# 0.2.6

* renovate

# 0.2.5

* renovate

# 0.2.4

* renovate

# 0.2.3

* renovate

# 0.2.2

* renovate

# 0.2.1

* renovate

# 0.2.0

* update to pydantic v2

# 0.1.57

* renovate

# 0.1.56

* renovate

# 0.1.55

* renovate

# 0.1.54

* renovate

# 0.1.53

* renovate

# 0.1.52

* renovate

# 0.1.51

* renovate

# 0.1.50

* renovate

# 0.1.49

* renovate

# 0.1.48

* renovate

# 0.1.47

* renovate

# 0.1.46

* renovate

# 0.1.45

* renovate

# 0.1.44

* renovate

# 0.1.43

* renovate

# 0.1.42

* renovate

# 0.1.41

* renovate

# 0.1.40

* renovate

# 0.1.39

* renovate

# 0.1.38

* renovate

# 0.1.37

* renovate

# 0.1.36

* renovate

# 0.1.35

* renovate

# 0.1.34

* renovate

# 0.1.33

* test data types besides str

# 0.1.32

* renovate

# 0.1.31

* renovate

# 0.1.30

* renovate

# 0.1.29

* renovate

# 0.1.28

* renovate

# 0.1.27

* renovate

# 0.1.26

* renovate

# 0.1.25

* renovate

# 0.1.24

* renovate

# 0.1.23

* renovate

# 0.1.22

* renovate

# 0.1.21

* renovate

# 0.1.20

* renovate

# 0.1.19

* renovate

# 0.1.18

* renovate

# 0.1.17

* renovate

# 0.1.16

* renovate

# 0.1.15

* renovate

# 0.1.14

* renovate

# 0.1.13

* renovate

# 0.1.12

* renovate

# 0.1.11

* renovate

# 0.1.10

* renovate

# 0.1.9

* renovate

# 0.1.8

* renovate

# 0.1.7

* renovate

# 0.1.6

* updated dependencies

 # 0.1.5

* updated ci

# 0.1.4

* added cors_allow_origins to settings again and made allow_credentials also configurable via cors_allow_credentials

# 0.1.3

* allow all origins, which is now possible because frontends do no more use client credentials (instead they explicitly 
use an authorization header)

# 0.1.2

* using FastAPI sub-apps for API versioning

# 0.1.1

* removed timestamps from logging
